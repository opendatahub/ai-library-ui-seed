const fs = require("fs");
const path = require("path");
const pLimit = require('p-limit');

const getEnv = require("./utils/getEnv");
const S3Storage = require("./utils/s3Storage");


const s3Endpoint = getEnv("S3_ENDPOINT");
const s3Bucket = getEnv("S3_BUCKET");
const s3Prefix = getEnv("S3_PREFIX");
const s3AccessKeyId = getEnv("S3_ACCESS_KEY_ID");
const s3SecretAccessKey = getEnv("S3_SECRET_ACCESS_KEY");
const s3ConcurrentUploadLimit = parseInt(process.env["S3_CONCURRENT_UPLOAD_LIMIT"] || 10);
const storage = new S3Storage(s3Endpoint, s3Bucket, s3Prefix, s3AccessKeyId, s3SecretAccessKey);

function readDir(localDir) {
  return new Promise((resolve, reject) => {
      fs.readdir(localDir, (error, files) => {
        if (error) {
          reject(error);
        } else {
          resolve(files);
        }
      });
    }
  );
}

async function readFileTree(localDir) {
  let allFiles = await readDir(localDir);
  let files = [];
  let subDirFiles = [];

  for (const fileName of allFiles) {
    const filePath = path.join(localDir, fileName);
    if (fs.lstatSync(filePath).isDirectory()) {
      subDirFiles.push(await readFileTree(filePath));
    } else {
      files.push(filePath);
    }
  }

  return files.concat(...subDirFiles);
}

async function uploadFileTree(localDir) {
  const limit = pLimit(s3ConcurrentUploadLimit);
  const files = await readFileTree(localDir);
  let promises = [];

  for (const filePath of files) {
    promises.push(limit(() => storage.uploadFile(filePath, filePath)));
  }

  return Promise.all(promises);
}

(async () => {
  let results;

  results = await uploadFileTree("./flake");
  for (let result of results) {
    console.log(result);
  }


  results = await uploadFileTree("./duplicate");
  for (let result of results) {
    console.log(result);
  }
})();