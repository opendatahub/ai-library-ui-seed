#!/bin/bash
set -x

source secrets

oc process -f openshift/ai-library-ui-seed.yml
  -p GIT_REF=${GIT_REF} \
  | oc delete -f -
